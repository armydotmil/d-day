var Beach = Backbone.Model.extend({
    urlRoot: 'https://www.army.mil/d-day/json/beaches.json'
});

var Beaches = Backbone.Collection.extend({
    model: Beach,
    url: 'https://www.army.mil/d-day/json/beaches.json'
});

var Division = Backbone.Model.extend({
    urlRoot: 'https://www.army.mil/d-day/json/divisions.json'
});

var Divisions = Backbone.Collection.extend({
    model: Beach,
    url: 'https://www.army.mil/d-day/json/divisions.json'
});

var beaches = new Beaches();
var divisions = new Divisions();

var beach_image_alt_texts = [
    "Image for Airborne Assault",
    "Image for Utah Beach",
    "Image for Omaha Beach",
    "Image for Gold Beach",
    "Image for Juno Beach",
    "Image for Sword Beach"
];

var BeachList = Backbone.View.extend({
    initialize: function() {
        this.listenTo(beaches, 'sync', this.load_beaches);
        beaches.fetch();
    },
    load_beaches: function() {
        for (var i = 0; i < beaches.length; i++) {
            var beach_name = beaches.models[i].get("name");
            var description = beaches.models[i].get("description");
            var image = beaches.models[i].get("image").replace('620', '492');
            var class_name = (i == 0) ? "active" : "";
            var list_item =
                $("<li>").prop({
                    "class": class_name
                }).append(
                    $("<a>").prop("href", "javascript:;").html(
                        $("<span>").append($("<h3>").html(beach_name)))
            );
            $("#beaches ul").append(list_item);
        }

        this.load_descriptions_and_images();
    },
    load_descriptions_and_images: function() {
        var beach_description, beach_name, beach_image, class_name = "";
        for (var i = 0; i < beaches.length; i++) {
            beach_description = beaches.models[i].get("description");
            beach_name = beaches.models[i].get("name");
            beach_image = beaches.models[i].get("image");
            class_name = (i === 0) ? " active" : (i == beaches.models.length - 1) ? " last" : "";

            var desc = $("<div>").prop({
                "class": "description" + class_name
            });

            var heading = $("<h3>").text(beach_name);

            var p = $("<p>").html(beach_description);

            var img = $("<img>").prop({
                "src": beach_image,
                "alt": beach_image_alt_texts[i]
            });

            $("#beach_descriptions").append(desc.append(heading, p, img));
        }
    },
    show_image: function(index) {
        $("#beach_descriptions .description").removeClass("active");
        $("#beach_descriptions .description").eq(index).addClass("active");
    }
});

var BeachItem = Backbone.View.extend({
    initialize: function() {},
    el: '#beach_list',
    events: {
        "click li a": "click"
    },
    click: function(e) {
        var index = $("#beach_list li a").index(e.currentTarget);
        beachList.show_image(index);
        $("#beach_list li").removeClass();
        $(e.currentTarget).parents("li").addClass("active");
    }
});

var beachList = new BeachList();
var beachItem = new BeachItem();



var DivisionList = Backbone.View.extend({
    initialize: function() {
        this.listenTo(divisions, 'sync', this.load_divisions);
        divisions.fetch();
    },
    load_divisions: function() {
        var divisions_grid = $("#divisions_grid");
        var divisions_allied_grid = $("#divisions_allied_grid");

        while (divisions.length > 0) {
            var divisions_grid_row = $("<div>").prop({
                "class": "divisions_grid_row row"
            });
            var allied_divisions_grid_row = $("<div>").prop({
                "class": "divisions_grid_row row"
            });
            for (var i = 0; i < 2; i++) {
                if (divisions.length > 0) {
                    var division = divisions.shift();
                    var cell = build_division_cell(division);
                    if (division.get('usa')) {
                        divisions_grid_row.append(cell);
                    } else {
                        allied_divisions_grid_row.append(cell);
                    }
                }
            }

            if (!divisions_grid_row.is(':empty')) {
                divisions_grid.append(divisions_grid_row);
            }
            if (!allied_divisions_grid_row.is(':empty')) {
                divisions_allied_grid.append(allied_divisions_grid_row);
            }
        }
    }
});

function build_division_cell(division) {
    var division_name = division.get('division_name');
    var division_url = division.get('division_url');
    var units = division.get('units');
    var division_image = division.get('division_image');

    var divisions_grid_cell = $("<div>").prop({
        "class": "divisions_grid_cell"
    });

    var alt_text = division_name + " Patch";
    if (division_name == "Non-Divisional Units") {
        alt_text = "Army Seal";
    }

    var img = $('<img>').prop({
        "src": "/e2/rv5_images/d-day/divisions/" + division_image,
        "class": "division_image",
        "alt": alt_text
    });

    var header = $("<h4>");

    //if(units.length > 0){
    var button = $('<span>').prop({
        "class": "expand"
    }).html(" +");

    var title = $('<span>').prop({
        "class": "division_name"
    }).html(division_name);
    var header_link = $("<a>").prop({
        'href': '#',
        'class': 'division_header_link'
    }).html(title);
    header_link.click(function(e) {
        e.preventDefault();
        //var button = $(this).children().find("span.expand");
        var list = $(this).parent().siblings("ul");

        if (list.is(":hidden")) {
            button.text(" -");
        } else {
            button.text(" +");
        }
        list.slideToggle();
    });
    header_link.append(button);
    header.append(header_link);
    //header.append(button);
    //}else{
    //	header.html(division_name);
    //}



    var unit_list = $("<ul>");

    //List a link, unless you have no link

    if (division_url != "#") {
        unit_list.append(
            $('<li>').append(
                $('<a>').prop({
                    "href": division_url,
                    "class": "division_url",
                    "target": "_blank"
                }).text("Division Website")
            )
        );
    }


    for (var i = 0; i < units.length; i++) {
        unit_list.append($('<li>').html(units[i]));
    }

    var header_list = $('<div>').prop({
        "class": "division_txt"
    }).append(header).append(unit_list);
    return divisions_grid_cell.append(img).append(header_list);
}

var divisionList = new DivisionList();

var media = {};

function get_new_image(view_width) {
    var new_image = "";

    if (view_width < 768) {
        new_image = "460.jpg";
    } else if (view_width < 992 && view_width >= 768) {
        new_image = "492.jpg";
    } else if (view_width >= 992) {
        new_image = "620.jpg";
    }

    $("#slides img, #wwii_slides img").each(function() {
        var old_src = $(this).prop("src");
        $(this).prop("src", old_src.replace(/\d\d\d\.jpg/g, new_image));
    });
}

function on_resize(view_width) {
    var change_image = false;
    if (parseInt(view_width, 10) >= 768 && media.start_width < 768) {
        change_image = true;
        media.start_width = parseInt($(window).width(), 10);
    } else if ((parseInt(view_width, 10) < 768 && media.start_width >= 768) || (parseInt(view_width, 10) >= 992 && media.start_width < 992)) {
        change_image = true;
        media.start_width = parseInt($(window).width(), 10);
    } else if (parseInt(view_width, 10) < 992 && media.start_width >= 992) {
        change_image = true;
        media.start_width = parseInt($(window).width(), 10);
    } else {
        change_image = false;
    }
    if (change_image) {
        get_new_image(view_width);
    }
}

var image_captions = [
    "Assault landing, one of the first waves at Omaha. The Coast Guard caption identifies the unit as Company E, 16th Infantry, 1st Infantry Division. Photo courtesy of Center of Military History.",
    "Supreme Allied Commander Gen. Dwight D. Eisenhower speaks and gives the order of the Day, \"Full victory - nothing else,\" with 101st Airborne Division paratroopers before they board airplanes and gliders to take part in a parachute assault. This was the first assault into Normandy as part of the Allied Invasion of Europe,\"D-Day,\" June 6, 1944. Photo by Moore.",
    "A fully equipped paratrooper boards an airplane that will drop him over the coast of Normandy for the Allied Invasion of Europe,\"D-Day,\" June 6, 1944. At approximately 2 a.m., Soldiers of the 82nd and 101st Airborne Divisions, as well as elements of a British airborne division; were dropped in vital areas to the rear of German coastal defenses, guarding the Normandy beaches from Cherbourg to Caen. By dawn, 1,136 heavy bombers of the Royal Air Force Bomber Command, had dropped 5,853 tons of bombs on selected coastal batteries, lining the Bay of the Seine between Cherbourg and Le Havre. Photo courtesy of Center of Military History.",
    "Army Air Corps photographers documented \"D-Day\" beach traffic, as photographed from a Ninth Air Force bomber, June 6, 1944. Note vehicle lanes leading away from the landing areas, and landing craft left aground by the tide. Photo courtesy of Center of Military History.",
    "American assault troops in a landing craft huddle behind the protective front of the craft as it nears a beachhead, on the Northern Coast of France. Smoke in the background is the Naval gunfire supporting the land, June 6, 1944. Photo courtesy of Center of Military History.",
    "In preparation for the invasion, artillery equipment is loaded aboard landing craft tanks at an English port in Brixham, England, June 1, 1944. Photo by Nehez.",
    "Paratroopers get final instructions before leaving for Normandy. Photo courtesy of National Archives.",
    "Carrying full equipment, American assault troops move onto Utah Beach on the northern coast of France. Landing crafts in the background, jams the harbor, June 6, 1944. Photo by Wall.",
    "Members of an American landing party lend helping hands to other members of their organization, whose landing craft was sunk by enemy action off the coast of France. These survivors reached Omaha Beach by using a life raft, June 6, 1944. Photo by Weintraub.",
    "Three Rhino barges and a petrol barge are being hammered by surf somewhere along the coast of France. Photo by Bacon.",
    "Photo taken two days after \"D-Day,\" and after the relief forces reached the Rangers at Point Du Hoe. The American flag had been spread out to stop fire of friendly tanks coming from inland. Some German prisoners are being moved in after their capture by the relieving forces. Photo courtesy of Center of Military History.",
    "This graphic tells the story of how the France beachhead was supplied on \"D-Day,\" June 6, 1944. Photo by Steck.",
    "The build-up of Omaha Beach. Reinforcements of men and equipment moving inland. Photo courtesy of Center of Military History.",
    "A medic of the 3rd Battalion, 16th Infantry Regiment, 1st Infantry Division, moves along a narrow strip of Omaha Beach, administering first aid to men wounded in the landing. The men, having gained the comparative safety offered by the chalk cliff at their backs, take a breather before moving into the interior of the continent, Collville, Sur-Mer, Normandy, France, June 6, 1944. Photo by Taylor.",
    "A convoy of landing crafts near the beach at Normandy, \"D-Day,\" June 6, 1944. Photo courtesy of Center of Military History.",
    "Soldiers and crewmen aboard a Coast Guard landing craft approach Normandy, \"D-Day,\" June 6, 1944. Photo courtesy of Center of Military History.",
    "Soldiers crowd a landing craft on their way to Omaha Beach during the Allied Invasion of Europe, \"D-Day,\" June 6, 1944. Photo courtesy of Center of Military History.",
    "U.S. Soldiers of the 8th Infantry Regiment, 4th Infantry Division, move over a seawall on Utah Beach during the Allied Invasion of Europe. Photo courtesy of Center of Military History.",
    "Medics attend to wounded Soldiers on Utah Beach in France during the Allied Invasion of Europe on D-Day, June 6, 1944. Photo courtesy of Center of Military History.",
    "Soldiers of the 16th Infantry Regiment, wounded while storming Omaha Beach, wait by the chalk cliffs for evacuation to a field hospital for treatment, \"D-Day,\" June 6, 1944. Photo courtesy of Center of Military History.",
    "Gliders fly supplies to Soldiers fighting on Utah Beach during the Allied Invasion of Europe, \"D-Day,\" June 6, 1944. Photo courtesy of Center of Military History.",
    "Soldiers in cargo vehicles move onto a beach in Normandy during the Allied Invasion of Europe, \"D-Day,\" June 6, 1944. After fierce fighting, the Allies established a foothold in northern France. Photo courtesy of Center of Military History.",
    "The spirit of the American Soldier: this beachhead is secure. Fellow Soldiers erected this monument to an American Soldier somewhere on the shell-blasted coast of Normandy. Photo courtesy of Center of Military History.",
    "Supreme Allied Commander Gen. Dwight D. Eisenhower, drafted the message, \"In case of failure,\" before the \"D-Day,\" invasion, the assault on Nazi-occupied France, June 5, 1944. He wrote this note, in case the operation were to fail. In the statement, he praised the men he commanded and accepted total responsibility for the failure the next day could bring. The only apparent hint of nerves on his part is his error in dating the note \"July 5\" instead of June 5. Photo courtesy of National Archives.",
    "Supreme Allied Commander Gen. Dwight D. Eisenhower, speaks with paratroopers of the 101st Airborne Division, just before they board their planes to participate in the first assault of the Normandy invasion, June 5, 1944. Photo courtesy of Center of Military History."
];

$(function() {
    media.start_width = $(window).width();
    $("#slides").slidesjs({
        width: 620,
        height: 417,
        navigation: {
            active: true
        },
        pagination: {
            active: false
        },
        callback: {
            loaded: function(num) {
                var caption = image_captions[parseInt(num - 1, 10)];
                var src = $(".slidesjs-slide").eq(parseInt(num - 1, 10)).prop("src");
                var original_src = src.replace(/\d\d\d\.jpg/g, "original.jpg");
                $("#slides .slidesjs-container").append(
                    $("<div>").prop({
                        "class": "slidesjs-caption"
                    }).append(
                        $("<p>").prop({
                            "class": "caption-text"
                        }).text(caption)
                    ).append(
                        $("<a>").prop({
                            "href": original_src,
                            "target": "_blank"
                        }).html("download image &#187;")
                    )
                ).append(
                    $("<a>").prop({
                        "class": "slidesjs-caption-button",
                        "href": "#"
                    }).text("Caption").click(function(e) {
                        e.preventDefault();
                        if ($(".slidesjs-caption").is(":hidden")) {
                            $(this).text("Close");
                        } else {
                            $(this).text("Caption");
                        }
                        $(".slidesjs-caption").slideToggle();
                    })
                );
            },
            complete: function(num) {
                var caption = image_captions[parseInt(num - 1, 10)];
                var src = $("#slides .slidesjs-slide").eq(parseInt(num - 1, 10)).prop("src");
                var original_src = src.replace(/\d\d\d\.jpg/g, "original.jpg");
                $(".caption-text").text(caption);
                $("#slides .slidesjs-caption a").prop({
                    "href": original_src
                });
            }
        }
    });
    $("#wwii_slides").slidesjs({
        width: 620,
        height: 417,
        navigation: {
            active: true
        },
        pagination: {
            active: false
        },
        callback: {
            loaded: function(num) {
                var src = $("#wwii_slides .slidesjs-slide").eq(parseInt(num - 1, 10)).prop("src");
                var original_src = src.replace(/\d\d\d\.jpg/g, "original.jpg");
                $("#wwii_slides .slidesjs-container").append(
                    $("<a>").prop({
                        "class": "slidesjs-caption-button",
                        "href": original_src,
                        "target": "_blank"
                    }).text("Download Image")
                );
            },
            complete: function(num) {
                var src = $("#wwii_slides .slidesjs-slide").eq(parseInt(num - 1, 10)).prop("src");
                var original_src = src.replace(/\d\d\d\.jpg/g, "original.jpg");
                $("#wwii_slides .slidesjs-caption-button").prop({
                    "href": original_src
                });
            }
        }
    });
    $(".slidesjs-previous,.slidesjs-next").append($("<span>"));



    var sjs = $("#slides").data('plugin_slidesjs');
    var sjs2 = $("#wwii_slides").data('plugin_slidesjs');

    $("#left_media_nav h3 a").click(function(e) {
        e.preventDefault();
        var active = $(e.target);
        var label = active.text().toLowerCase().replace(' ', '_');
        active.parents('li').siblings().find('h3 a').removeClass();
        $(".section_container").removeClass("active");
        active.addClass("active");
        if (active.text() === "Images") {
            $("#slides").parent().addClass('active');
            sjs.update();
        } else if (active.text() === "Videos") {
            $("#video_player").addClass('active');
        } else {
            $("#wwii_slides").parent().addClass('active');
            sjs2.update();
        }
        if (typeof ga === "function") {
            ga('send', 'event', 'navigation', 'click', label)
        }
    });

    dday_player.add_player();

    $(window).on('resize', function() {
        on_resize($(this).width());
    });

    //GA is triggered when users scroll on a section
    //Added by Fiston Madimba on July 17 2014

    //Multimedia Section 
    $('#media').waypoint(function() {
        //e.preventDefault();
        if (_.isFunction(ga)) {
            ga("send", "event", "history", "scroll", "multimedia");
        }
    });
    //US Army Divisions Section
    $('#divisions').waypoint(function() {
        //e.preventDefault();
        if (_.isFunction(ga)) {
            ga("send", "event", "history", "scroll", "us army dvisions");
        }
    });
    //US Allies Section
    $('#divisions_allied_grid').waypoint(function() {
        //e.preventDefault();
        if (_.isFunction(ga)) {
            ga("send", "event", "history", "scroll", "us allies");
        }
    });
    //Airborne and Beach Assault Section
    $('#beaches').waypoint(function() {
        //e.preventDefault();
        if (_.isFunction(ga)) {
            ga("send", "event", "history", "scroll", "airborne and beach assault");
        }
    });
    //Medal of Honor Section
    $('#honor').waypoint(function() {
        //e.preventDefault();
        if (_.isFunction(ga)) {
            ga("send", "event", "history", "scroll", "medal of honor");
        }
    });


    $("#slides").on('click', '.slidesjs-caption a', function(e) {
        if (_.isFunction(ga)) {
            var href = $(e.currentTarget).attr('href');
            var file_type = href.split('.').pop();
            ga("send", "event", "downloads", file_type, href);
        }
        return true;
    });
});


// Generated by CoffeeScript 1.6.2
/*!
jQuery Waypoints - v2.0.5
Copyright (c) 2011-2014 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/jquery-waypoints/blob/master/licenses.txt
*/


(function() {
    var __indexOf = [].indexOf || function(item) {
            for (var i = 0, l = this.length; i < l; i++) {
                if (i in this && this[i] === item) return i;
            }
            return -1;
        },
        __slice = [].slice;

    (function(root, factory) {
        if (typeof define === 'function' && define.amd) {
            return define('waypoints', ['jquery'], function($) {
                return factory($, root);
            });
        } else {
            return factory(root.jQuery, root);
        }
    })(window, function($, window) {
            var $w, Context, Waypoint, allWaypoints, contextCounter, contextKey, contexts, isTouch, jQMethods, methods, resizeEvent, scrollEvent, waypointCounter, waypointKey, wp, wps;

            $w = $(window);
            isTouch = __indexOf.call(window, 'ontouchstart') >= 0;
            allWaypoints = {
                horizontal: {},
                vertical: {}
            };
            contextCounter = 1;
            contexts = {};
            contextKey = 'waypoints-context-id';
            resizeEvent = 'resize.waypoints';
            scrollEvent = 'scroll.waypoints';
            waypointCounter = 1;
            waypointKey = 'waypoints-waypoint-ids';
            wp = 'waypoint';
            wps = 'waypoints';
            Context = (function() {
                function Context($element) {
                    var _this = this;

                    this.$element = $element;
                    this.element = $element[0];
                    this.didResize = false;
                    this.didScroll = false;
                    this.id = 'context' + contextCounter++;
                    this.oldScroll = {
                        x: $element.scrollLeft(),
                        y: $element.scrollTop()
                    };
                    this.waypoints = {
                        horizontal: {},
                        vertical: {}
                    };
                    this.element[contextKey] = this.id;
                    contexts[this.id] = this;
                    $element.bind(scrollEvent, function() {
                        var scrollHandler;

                        if (!(_this.didScroll || isTouch)) {
                            _this.didScroll = true;
                            scrollHandler = function() {
                                _this.doScroll();
                                return _this.didScroll = false;
                            };
                            return window.setTimeout(scrollHandler, $[wps].settings.scrollThrottle);
                        }
                    });
                    $element.bind(resizeEvent, function() {
                        var resizeHandler;

                        if (!_this.didResize) {
                            _this.didResize = true;
                            resizeHandler = function() {
                                $[wps]('refresh');
                                return _this.didResize = false;
                            };
                            return window.setTimeout(resizeHandler, $[wps].settings.resizeThrottle);
                        }
                    });
                }

                Context.prototype.doScroll = function() {
                    var axes,
                        _this = this;

                    axes = {
                        horizontal: {
                            newScroll: this.$element.scrollLeft(),
                            oldScroll: this.oldScroll.x,
                            forward: 'right',
                            backward: 'left'
                        },
                        vertical: {
                            newScroll: this.$element.scrollTop(),
                            oldScroll: this.oldScroll.y,
                            forward: 'down',
                            backward: 'up'
                        }
                    };
                    if (isTouch && (!axes.vertical.oldScroll || !axes.vertical.newScroll)) {
                        $[wps]('refresh');
                    }
                    $.each(axes, function(aKey, axis) {
                        var direction, isForward, triggered;

                        triggered = [];
                        isForward = axis.newScroll > axis.oldScroll;
                        direction = isForward ? axis.forward : axis.backward;
                        $.each(_this.waypoints[aKey], function(wKey, waypoint) {
                            var _ref, _ref1;

                            if ((axis.oldScroll < (_ref = waypoint.offset) && _ref <= axis.newScroll)) {
                                return triggered.push(waypoint);
                            } else if ((axis.newScroll < (_ref1 = waypoint.offset) && _ref1 <= axis.oldScroll)) {
                                return triggered.push(waypoint);
                            }
                        });
                        triggered.sort(function(a, b) {
                            return a.offset - b.offset;
                        });
                        if (!isForward) {
                            triggered.reverse();
                        }
                        return $.each(triggered, function(i, waypoint) {
                            if (waypoint.options.continuous || i === triggered.length - 1) {
                                return waypoint.trigger([direction]);
                            }
                        });
                    });
                    return this.oldScroll = {
                        x: axes.horizontal.newScroll,
                        y: axes.vertical.newScroll
                    };
                };

                Context.prototype.refresh = function() {
                    var axes, cOffset, isWin,
                        _this = this;

                    isWin = $.isWindow(this.element);
                    cOffset = this.$element.offset();
                    this.doScroll();
                    axes = {
                        horizontal: {
                            contextOffset: isWin ? 0 : cOffset.left,
                            contextScroll: isWin ? 0 : this.oldScroll.x,
                            contextDimension: this.$element.width(),
                            oldScroll: this.oldScroll.x,
                            forward: 'right',
                            backward: 'left',
                            offsetProp: 'left'
                        },
                        vertical: {
                            contextOffset: isWin ? 0 : cOffset.top,
                            contextScroll: isWin ? 0 : this.oldScroll.y,
                            contextDimension: isWin ? $[wps]('viewportHeight') : this.$element.height(),
                            oldScroll: this.oldScroll.y,
                            forward: 'down',
                            backward: 'up',
                            offsetProp: 'top'
                        }
                    };
                    return $.each(axes, function(aKey, axis) {
                        return $.each(_this.waypoints[aKey], function(i, waypoint) {
                            var adjustment, elementOffset, oldOffset, _ref, _ref1;

                            adjustment = waypoint.options.offset;
                            oldOffset = waypoint.offset;
                            elementOffset = $.isWindow(waypoint.element) ? 0 : waypoint.$element.offset()[axis.offsetProp];
                            if ($.isFunction(adjustment)) {
                                adjustment = adjustment.apply(waypoint.element);
                            } else if (typeof adjustment === 'string') {
                                adjustment = parseFloat(adjustment);
                                if (waypoint.options.offset.indexOf('%') > -1) {
                                    adjustment = Math.ceil(axis.contextDimension * adjustment / 100);
                                }
                            }
                            waypoint.offset = elementOffset - axis.contextOffset + axis.contextScroll - adjustment;
                            if ((waypoint.options.onlyOnScroll && (oldOffset != null)) || !waypoint.enabled) {
                                return;
                            }
                            if (oldOffset !== null && (oldOffset < (_ref = axis.oldScroll) && _ref <= waypoint.offset)) {
                                return waypoint.trigger([axis.backward]);
                            } else if (oldOffset !== null && (oldOffset > (_ref1 = axis.oldScroll) && _ref1 >= waypoint.offset)) {
                                return waypoint.trigger([axis.forward]);
                            } else if (oldOffset === null && axis.oldScroll >= waypoint.offset) {
                                return waypoint.trigger([axis.forward]);
                            }
                        });
                    });
                };

                Context.prototype.checkEmpty = function() {
                    if ($.isEmptyObject(this.waypoints.horizontal) && $.isEmptyObject(this.waypoints.vertical)) {
                        this.$element.unbind([resizeEvent, scrollEvent].join(' '));
                        return delete contexts[this.id];
                    }
                };

                return Context;

            })();
            Waypoint = (function() {
                function Waypoint($element, context, options) {
                    var idList, _ref;

                    if (options.offset === 'bottom-in-view') {
                        options.offset = function() {
                            var contextHeight;

                            contextHeight = $[wps]('viewportHeight');
                            if (!$.isWindow(context.element)) {
                                contextHeight = context.$element.height();
                            }
                            return contextHeight - $(this).outerHeight();
                        };
                    }
                    this.$element = $element;
                    this.element = $element[0];
                    this.axis = options.horizontal ? 'horizontal' : 'vertical';
                    this.callback = options.handler;
                    this.context = context;
                    this.enabled = options.enabled;
                    this.id = 'waypoints' + waypointCounter++;
                    this.offset = null;
                    this.options = options;
                    context.waypoints[this.axis][this.id] = this;
                    allWaypoints[this.axis][this.id] = this;
                    idList = (_ref = this.element[waypointKey]) != null ? _ref : [];
                    idList.push(this.id);
                    this.element[waypointKey] = idList;
                }

                Waypoint.prototype.trigger = function(args) {
                    if (!this.enabled) {
                        return;
                    }
                    if (this.callback != null) {
                        this.callback.apply(this.element, args);
                    }
                    if (this.options.triggerOnce) {
                        return this.destroy();
                    }
                };

                Waypoint.prototype.disable = function() {
                    return this.enabled = false;
                };

                Waypoint.prototype.enable = function() {
                    this.context.refresh();
                    return this.enabled = true;
                };

                Waypoint.prototype.destroy = function() {
                    delete allWaypoints[this.axis][this.id];
                    delete this.context.waypoints[this.axis][this.id];
                    return this.context.checkEmpty();
                };

                Waypoint.getWaypointsByElement = function(element) {
                    var all, ids;

                    ids = element[waypointKey];
                    if (!ids) {
                        return [];
                    }
                    all = $.extend({}, allWaypoints.horizontal, allWaypoints.vertical);
                    return $.map(ids, function(id) {
                        return all[id];
                    });
                };

                return Waypoint;

            })();
            methods = {
                init: function(f, options) {
                    var _ref;

                    options = $.extend({}, $.fn[wp].defaults, options);
                    if ((_ref = options.handler) == null) {
                        options.handler = f;
                    }
                    this.each(function() {
                        var $this, context, contextElement, _ref1;

                        $this = $(this);
                        contextElement = (_ref1 = options.context) != null ? _ref1 : $.fn[wp].defaults.context;
                        if (!$.isWindow(contextElement)) {
                            contextElement = $this.closest(contextElement);
                        }
                        contextElement = $(contextElement);
                        context = contexts[contextElement[0][contextKey]];
                        if (!context) {
                            context = new Context(contextElement);
                        }
                        return new Waypoint($this, context, options);
                    });
                    $[wps]('refresh');
                    return this;
                },
                disable: function() {
                    return methods._invoke.call(this, 'disable');
                },
                enable: function() {
                    return methods._invoke.call(this, 'enable');
                },
                destroy: function() {
                    return methods._invoke.call(this, 'destroy');
                },
                prev: function(axis, selector) {
                    return methods._traverse.call(this, axis, selector, function(stack, index, waypoints) {
                        if (index > 0) {
                            return stack.push(waypoints[index - 1]);
                        }
                    });
                },
                next: function(axis, selector) {
                    return methods._traverse.call(this, axis, selector, function(stack, index, waypoints) {
                        if (index < waypoints.length - 1) {
                            return stack.push(waypoints[index + 1]);
                        }
                    });
                },
                _traverse: function(axis, selector, push) {
                    var stack, waypoints;

                    if (axis == null) {
                        axis = 'vertical';
                    }
                    if (selector == null) {
                        selector = window;
                    }
                    waypoints = jQMethods.aggregate(selector);
                    stack = [];
                    this.each(function() {
                        var index;

                        index = $.inArray(this, waypoints[axis]);
                        return push(stack, index, waypoints[axis]);
                    });
                    return this.pushStack(stack);
                },
                _invoke: function(method) {
                    this.each(function() {
                        var waypoints;

                        waypoints = Waypoint.getWaypointsByElement(this);
                        return $.each(waypoints, function(i, waypoint) {
                            waypoint[method]();
                            return true;
                        });
                    });
                    return this;
                }
            };
            $.fn[wp] = function() {
                var args, method;

                method = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
                if (methods[method]) {
                    return methods[method].apply(this, args);
                } else if ($.isFunction(method)) {
                    return methods.init.apply(this, arguments);
                } else if ($.isPlainObject(method)) {
                    return methods.init.apply(this, [null, method]);
                } else if (!method) {
                    return $.error("jQuery Waypoints needs a callback function or handler option.");
                } else {
                    return $.error("The " + method + " method does not exist in jQuery Waypoints.");
                }
            };
            $.fn[wp].defaults = {
                context: window,
                continuous: true,
                enabled: true,
                horizontal: false,
                offset: 0,
                triggerOnce: false
            };
            jQMethods = {
                refresh: function() {
                    return $.each(contexts, function(i, context) {
                        return context.refresh();
                    });
                },
                viewportHeight: function() {
                    var _ref;

                    return (_ref = window.innerHeight) != null ? _ref : $w.height();
                },
                aggregate: function(contextSelector) {
                    var collection, waypoints, _ref;

                    collection = allWaypoints;
                    if (contextSelector) {
                        collection = (_ref = contexts[$(contextSelector)[0][contextKey]]) != null ? _ref.waypoints : void 0;
                    }
                    if (!collection) {
                        return [];
                    }
                    waypoints = {
                        horizontal: [],
                        vertical: []
                    };
                    $.each(waypoints, function(axis, arr) {
                        $.each(collection[axis], function(key, waypoint) {
                            return arr.push(waypoint);
                        });
                        arr.sort(function(a, b) {
                            return a.offset - b.offset;
                        });
                        waypoints[axis] = $.map(arr, function(waypoint) {
                            return waypoint.element;
                        });
                        return waypoints[axis] = $.unique(waypoints[axis]);
                    });
                    return waypoints;
                },
                above: function(contextSelector) {
                    if (contextSelector == null) {
                        contextSelector = window;
                    }
                    return jQMethods._filter(contextSelector, 'vertical', function(context, waypoint) {
                        return waypoint.offset <= context.oldScroll.y;
                    });
                },
                below: function(contextSelector) {
                    if (contextSelector == null) {
                        contextSelector = window;
                    }
                    return jQMethods._filter(contextSelector, 'vertical', function(context, waypoint) {
                        return waypoint.offset > context.oldScroll.y;
                    });
                },
                left: function(contextSelector) {
                    if (contextSelector == null) {
                        contextSelector = window;
                    }
                    return jQMethods._filter(contextSelector, 'horizontal', function(context, waypoint) {
                        return waypoint.offset <= context.oldScroll.x;
                    });
                },
                right: function(contextSelector) {
                    if (contextSelector == null) {
                        contextSelector = window;
                    }
                    return jQMethods._filter(contextSelector, 'horizontal', function(context, waypoint) {
                        return waypoint.offset > context.oldScroll.x;
                    });
                },
                enable: function() {
                    return jQMethods._invoke('enable');
                },
                disable: function() {
                    return jQMethods._invoke('disable');
                },
                destroy: function() {
                    return jQMethods._invoke('destroy');
                },
                extendFn: function(methodName, f) {
                    return methods[methodName] = f;
                },
                _invoke: function(method) {
                    var waypoints;

                    waypoints = $.extend({}, allWaypoints.vertical, allWaypoints.horizontal);
                    return $.each(waypoints, function(key, waypoint) {
                        waypoint[method]();
                        return true;
                    });
                },
                _filter: function(selector, axis, test) {
                    var context, waypoints;

                    context = contexts[$(selector)[0][contextKey]];
                    if (!context) {
                        return [];
                    }
                    waypoints = [];
                    $.each(context.waypoints[axis], function(i, waypoint) {
                        if (test(context, waypoint)) {
                            return waypoints.push(waypoint);
                        }
                    });
                    waypoints.sort(function(a, b) {
                        return a.offset - b.offset;
                    });
                    return $.map(waypoints, function(waypoint) {
                        return waypoint.element;
                    });
                }
            };
            $[wps] = function() {
                var args, method;

                method = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
                if (jQMethods[method]) {
                    return jQMethods[method].apply(null, args);
                } else {
                    return jQMethods.aggregate.call(null, method);
                }
            };
            $[wps].settings = {
                resizeThrottle: 100,
                scrollThrottle: 30
            };
            return $w.on('load.waypoints', function() {
                return $[wps]('refresh');
            });
        });

}).call(this);
