$(function() {
    $("#header").load("header.html", function() {
        $("#menu_button").click(function() {
            $("#drop_down_menu").slideToggle();
            return false;
        });
        //for event tracking on the header bar
        $("#header a").on("click", function() {
            if (_.isFunction(ga)) {
                ga("send", "event", "dday_menu", "click", $(this).prop('href'));
            }
            return true;
        });
    });
    $("#footer").load("footer.html", function() {
        $(".resource_header").on('click', function() {
            if ($(this).find("span").is(":visible")) {
                var lists = $(this).parent().siblings().find("ul");
                var next_list = $(this).next('ul');
                if (next_list.hasClass("active")) {
                    next_list.removeClass("active");
                } else {
                    lists.removeClass("active");
                    next_list.addClass("active");
                }
            }
        });
    });

    $(window).scroll(function() {
        var collapse_point = 50;

        if ($(window).scrollTop() > collapse_point) {
            $("#header").addClass("collapsed");
            $("#featureBar").addClass("fb_collapsed");
        } else {
            $("#header").removeClass("collapsed");
            $("#featureBar").removeClass("fb_collapsed");
        }
    });

    $("#slides,#wwii_slides").on('click', '.slidesjs-previous', function(e) {
        e.preventDefault();
        if (_.isFunction(ga)) {
            ga("send", "event", "slideshow", "click", "nav_left");
        }
    });

    $("#slides,#wwii_slides").on('click', '.slidesjs-next', function(e) {
        e.preventDefault();
        if (_.isFunction(ga)) {
            ga("send", "event", "slideshow", "click", "nav_right");
        }
    });

    $("#slides,#wwii_slides").on('click', '.slidesjs-caption-button', function(e) {
        var button = $(e.currentTarget).text().toLowerCase();
        if (_.isFunction(ga)) {
            if (button === "close") {
                ga("send", "event", "slideshow", "click", "caption_open");
            } else if (button === "caption") {
                ga("send", "event", "slideshow", "click", "caption_close");
            } else if (button === "download image") {
                var href = $(e.currentTarget).attr('href');
                var file_type = href.split('.').pop();
                ga("send", "event", "downloads", file_type, href);
            }
        }
        return true;
    });

    $("#slides").on('click', '.slidesjs-caption a', function(e) {
        if (_.isFunction(ga)) {
            var href = $(e.currentTarget).attr('href');
            var file_type = href.split('.').pop();
            ga("send", "event", "downloads", file_type, href);
        }
        return true;
    });

    $("#divisions_grid,#divisions_allied_grid").on('click', '.divisions_grid_cell .expand', function(e) {
        e.preventDefault();
        var divison = $(e.currentTarget).siblings('.division_name:first').text();
        if (_.isFunction(ga)) {
            if ($(e.currentTarget).hasClass('open')) {
                $(e.currentTarget).removeClass('open');
                ga("send", "event", "divisions", "open_minus", divison);
            } else {
                $(e.currentTarget).addClass('open');
                ga("send", "event", "divisions", "open_plus", divison);
            }
        }
    });

    $("#divisions_grid,#divisions_allied_grid").on('click', '.divisions_grid_cell .division_name', function(e) {
        e.preventDefault();
        var divison = $(e.currentTarget).text();
        if (_.isFunction(ga)) {
            if ($(e.currentTarget).hasClass('open')) {
                $(e.currentTarget).removeClass('open');
                ga("send", "event", "divisions", "close_text", divison);
            } else {
                $(e.currentTarget).addClass('open');
                ga("send", "event", "divisions", "open_text", divison);
            }
        }
    });

    $("#beach_list").on('click', 'li', function(e) {
        e.preventDefault();
        var beach = $(e.currentTarget).find('h3:first').text();
        if (_.isFunction(ga)) {
            ga("send", "event", "beaches", "click", beach);
        }
    });

    //external 
    $("a").click(function() {
        var microSiteUrl = $(this).attr("href");
        var domain = document.domain;
        var lldomain = "usarmy.vo.llnwd.net";
        var pound = "#";
        var js = "javascript:;";
        if ((microSiteUrl.search(pound) < 0) && (microSiteUrl.search(domain) < 0) && (microSiteUrl.search(lldomain) < 0) && (microSiteUrl.search(js) < 0) && _.isFunction(ga) && !_.isUndefined(microSiteUrl) && microSiteUrl != "") {
            ga('send', 'event', 'outbound', 'click', microSiteUrl);
        }
    });

    if (typeof addthis != "undefined") {
        addthis.layers({
            'theme': 'transparent',
            'share': {
                'position': 'left',
                'services': 'facebook,twitter,google_plusone_share,pinterest',
                'offset': {
                    'top': '220px'
                }
            },
            'thankyou': 'false',
            'responsive': {
                'maxWidth': '768px'
            },
            'mobile': {
                'buttonBarPosition': 'bottom',
                'buttonBarTheme': 'light',
                'mobile': true
            }
        });
    }
});
