var keyword = 'target_d-day_images';

var _Image = Backbone.Model.extend({
    urlRoot: 'https://www.army.mil/api/packages/getpackagesbykeywords?keywords=' + keyword
});

var Images = Backbone.Collection.extend({
    model: _Image,
    url: 'https://www.army.mil/api/packages/getpackagesbykeywords?keywords=' + keyword
});

var images = new Images();

var imageCollection = [];

var ImageGallery = Backbone.View.extend({
    initialize: function() {
        this.listenTo(images, 'sync', this.load_articles);
        images.fetch();
    },
    images_loaded: 0,
    load_articles: function(){
        for(var i = 0; i < images.length; i++ ) {
            imageCollection = imageCollection.concat(images.shift().get('images'));
        }
        
        for (var i = 0; i < 2; i++) {
            this.load_images();
        }
    },
    load_images: function() {
        var image_gallery_images = $('<div>').prop({
            'class': 'image_gallery_images'
        });

        var clear = $('<div>').prop({
            'class': 'clear'
        });
        
        for (var i = 0; i < 4; i++) {
            if (imageCollection.length > 0) {
                image_gallery_images.append(build_image_cell(imageCollection.shift()));
            } else {
                $("#image_gallery .button").hide();
            }
        }
        $("#image_gallery_section").append(image_gallery_images).append(clear);
    }
});

function build_image_cell(image) {
    if(image === undefined){return;}
    var window_width = parseInt($(window).width());
    var image_url = image.url.replace("original", 'size2');

    if (window_width < 769) {
        image_url = image.url.replace("original", 'size1');
    }

    var alt = image.alt;

    var title = image.title;
    var max_str_length = 50;
    if (title.length > max_str_length) {
        title = title.substring(0, max_str_length);
        title += "...";
    }
    var article_link = "https://www.army.mil/media/" + image.id;
    var original_image = image.url;
    var image_gallery_image = $('<div>').prop({
        'class': 'image_gallery_image'
    });

    var limage_link = $("<a>").prop({
        'target': '_blank',
        'href': article_link
    });

    var limage = $('<img>').prop({
        'src': image_url,
        'alt': alt
    });

    var image_gallery_image_link = $('<a>').prop({
        'class': 'image_gallery_image_link',
        'target': '_blank',
        'href': original_image
    }).html("DOWNLOAD IMAGE &#187;").on('click', function() {
        if (typeof ga === "function") {
            ga('send', 'event', 'downloads', 'jpg', original_image);
        }
        return true;
    });
    return image_gallery_image.append(limage_link.append(limage)).append(image_gallery_image_link);
}

$(function() {

    var imageGallery = new ImageGallery();
    var articleGallery = new ArticleGallery({
        rows: 2,
        offset: 1,
        count: 100
    });

    $("#load_more_images_button").click(function() {
        imageGallery.images_loaded++;
        for (var i = 0; i < 2; i++) {
            imageGallery.load_images();
        }
        if (typeof ga === "function") {
            ga('send', 'event', 'navigation', 'click', 'more_images_' + imageGallery.images_loaded);
        }
    });

    $("#load_more_news_button").click(function() {
        articleGallery.articles_loaded++;
        for (var i = 0; i < 2; i++) {
            articleGallery.load_articles();
        }
        if (typeof ga === "function") {
            ga('send', 'event', 'navigation', 'click', 'more_articles_' + articleGallery.articles_loaded);
        }
    });
});
