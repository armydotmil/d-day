function onYouTubeIframeAPIReady() {
    var player;
    player = new YT.Player('dday_player', {
        width: 620,
        height: 417,
        origin: 'https://www.army.mil/d-day/',
        playerVars: {
            listType: 'playlist',
            list: 'PLitjSv4SbrkxgcfKV5KFSe5Vi1FfQjpKn'
        }
    });
}

var dday_player = {
    add_player: function() {
        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }
}
