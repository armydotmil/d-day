$(function() {
    var articleGallery = new ArticleGallery({
        rows: 1,
        offset: 1,
        count: 3
    });
    var call_to_action_arr = {
        1: {
            "url": "/e2/rv5_downloads/d-day/the-meaning-of-dday-fact.pdf",
            "text": '"Many explanations have been given for the meaning of D-Day, June 6, 1944, the day the Allies invaded Normandy from England during World War II..." - "War Slang" by Paul Dickson',
            "button_text": "Learn the context of the 'D' in D-Day"
        },
        2: {
            "url": "https://www.army.mil/d-day/history.html#honor",
            "text": "The Medal of Honor is the nation's highest medal for valor in combat that can be awarded to members of the armed forces.",
            "button_text": "View the D-Day Medal of Honor citations"
		},
        3: {
            "url": "history.html?from=dday_rotator_eisenhower#video",
            "text": 'Gen. Dwight D. Eisenhower encouraged Allied soldiers taking part in the D-day invasion of June 6, 1944, reminding them, "The eyes of the world are upon you," before they embarked on " a great crusade."',
            "button_text": "Listen to Gen. Eisenhower\'s D-Day Message"

        },
        4: {
            "url": "/e2/rv5_downloads/d-day/continental_edition_4_july_1944.pdf",
            "text": "Shortly after the Allied Powers launched the campaign to free Nazi-occupied Europe, Stars and Stripes newspaper began printing the Continental Edition, to tell the stories of the service members.",
            "button_text": "Download the Continental Edition (PDF - 3.8 MB)"

        },
    };
    if ($("#slides img").length > 1) {
        $("#slides").slidesjs({
            width: 940,
            height: 632,
            navigation: {
                active: false
            },
            pagination: {
                active: true,
                // [boolean] Create pagination items.
                // You cannot use your own pagination. Sorry.
                effect: "slide"
                // [string] Can be either "slide" or "fade".
            },
            play: {
                active: true,
                // [boolean] Generate the play and stop buttons.
                // You cannot use your own buttons. Sorry.
                effect: "slide",
                // [string] Can be either "slide" or "fade".
                interval: 5000,
                // [number] Time spent on each slide in milliseconds.
                auto: true,
                // [boolean] Start playing the slideshow on load.
                swap: true,
                // [boolean] show/hide stop and play buttons
                pauseOnHover: true,
                // [boolean] pause a playing slideshow on hover
                restartDelay: 2500
                // [number] restart delay on inactive slideshow
            },
            callback: {
                loaded: function(number) {

                    var call_to_action = $('<div>').prop({
                        'class': 'slidesjs-call-to-action'
                    });

                    var more_info = $('<a>').prop({
                        'href': call_to_action_arr[1].url,
                        'class': 'slidesjs-call-to-action-button'
                    }).on('click', function() {
                        if ($(this).prop('href').match(/\.pdf/g)) {
                            if (typeof ga === "function") {
                                ga('send', 'event', 'downloads', 'pdf', $(this).prop('href'));
                            }
                        }
                        return true;
                    });
                    more_info.html(call_to_action_arr[1].button_text);
                    var text = $('<p>').html(call_to_action_arr[1].text);
                    call_to_action.append(text);
                    call_to_action.append(more_info);
                    $(".slidesjs-container").append(call_to_action);
                    $('.slidesjs-pagination-item a').on('click', function(e) {
                        var pip = parseInt($(e.currentTarget).parent('li').index() + 1, 10);
                        if (typeof ga === "function") {
                            ga('send', 'event', 'navigation', 'click', 'pip' + pip);
                        }
                        return true;
                    });
                },
                complete: function(number) {
                    $('.slidesjs-call-to-action p').html(call_to_action_arr[number].text);
                    $('.slidesjs-call-to-action-button').attr('href', call_to_action_arr[number].url);
                    if (number == 4) {
                        $('.slidesjs-call-to-action-button').attr('target', '_blank');
                    }
                    $('.slidesjs-call-to-action-button').html(call_to_action_arr[number].button_text);
                }
            }
        });
    }
    $('#news_more_button').click(function() {
        if (typeof ga === "function") {
            ga('send', 'event', 'navigation', 'click', 'more_news');
        }
        return true;
    });
});
