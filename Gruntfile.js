module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jslint: {
            client: {
                src: [
                    'src/_js/**/*.js'
                ],
                directives: {
                    browser: true
                },
                exclude: [
                    'src/_js/backbone-min.js',
                    'src/_js/modernizr.*.js',
                    'src/_js/owl.carousel.custom.min.js',
                    'src/_js/spin.min.js',
                    'src/_js/underscore-min.js',
                    'src/_js/waypoints.min.js',
                    'src/_js/youtube_iframe_api.js'
                ],
                options: {

                }
            }
        },
        jshint: {
            all: [
                'src/_js/**/*.js'
            ],
            options: {
                ignores: [
                    'src/_js/backbone-min.js',
                    'src/_js/modernizr.*.js',
                    'src/_js/owl.carousel.custom.min.js',
                    'src/_js/spin.min.js',
                    'src/_js/underscore-min.js',
                    'src/_js/waypoints.min.js',
                    'src/_js/youtube_iframe_api.js'
                ]
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'src/to_origin/rv5_css/d-day/style.css': 'src/_scss/style.scss'
                }

            }
        },
        jsbeautifier: {
            files: ['src/**/*.html'],
            options: {}
        },
        accessibility: {
            options: {
                accessibilityLevel: 'WCAG2A'
            },
            test: {
                src: ['src/**/*.html']
            }
        },
        curl: {
            'build/rv5_images.zip': 'https://bitbucket.org/armydotmil/<%= pkg.name %>/downloads/rv5_images.zip',
        },
        zip: {
            'build/rv5_images.zip': ['src/to_origin/rv5_images/**/*']
        },
        unzip: {
            highlight: {
                src: ['build/rv5_images.zip'],
                dest: '.'
            }
        },
        replace: {
            cdn: {
                src: ['src/**/*.html', 'src/_js/**/*.js', 'src/_scss/**/*.scss'],
                overwrite: true,
                replacements: [{
                    from: 'http://frontend.ardev.us/development/<%= pkg.name %>/to_origin/',
                    to: '/e2/'
                }, {
                    from: 'http://frontend.ardev.us/api/',
                    to: 'https://www.army.mil/api/'
                }]
            },
            dev: {
                src: ['src/**/*.html', 'src/_js/**/*.js', 'src/_scss/**/*.scss'],
                overwrite: true,
                replacements: [{
                    from: /\/e2\/(?!rv5_js\/3rdparty|rv5_js\/main|rv5_js\/features|rv5_css\/features|rv5_images\/features)/g,
                    to: 'http://frontend.ardev.us/development/<%= pkg.name %>/to_origin/'
                }, {
                    from: 'https://www.army.mil/api/',

                    to: 'http://frontend.ardev.us/api/'

                }]
            }
        },
        'http-server': {
            'dev': {
                root: 'src/',
                port: 8282,
                host: "0.0.0.0",
                ext: "html",
                runInBackground: false
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                mangle: true,
                compress: true,
                beautify: false
            },
            build: {
                files: {
                    'src/to_origin/rv5_js/d-day/header_footer.min.js': 'src/_js/header_footer.full.js',
                    'src/to_origin/rv5_js/d-day/index.min.js': [
                        'src/_js/jquery.slides.js',
                        'src/_js/article.full.js',
                        'src/_js/index.full.js',
                        'src/_js/header_footer.full.js'
                    ],
                    'src/to_origin/rv5_js/d-day/history.min.js': [
                        'src/_js/waypoints.js',
                        'src/_js/jquery.slides.js',
                        'src/_js/swfobject.js',
                        'src/_js/video_player.full.js',
                        'src/_js/media.full.js',
                        'src/_js/header_footer.full.js',
                    ],
                    'src/to_origin/rv5_js/d-day/news.min.js': [
                        'src/_js/article.full.js',
                        'src/_js/news.full.js',
                        'src/_js/header_footer.full.js',
                    ]
                }
            }
        },
        'sftp-deploy': {
            build: {
                auth: {
                    host: 'frontend.ardev.us',
                    authKey: 'privateKey'
                },
                cache: 'sftpCache.json',
                src: 'src/',
                dest: '/www/development/<%= pkg.name %>',
                exclusions: ['build/', 'node_module/', 'Gruntfile.js', 'package.json', 'readme.md', '.sass-cache', '.git', '.gitignore'],
                serverSep: '/',
                concurrency: 4,
                progress: true
            }
        },

        watch: {
            scripts: {
                files: ['src/_js/**/*.js'],
                tasks: ['uglify'],
                options: {
                    spawn: false,
                },
            },
        },
        requirejs: {
            compile: {
                options: {
                    baseUrl: "src/to_origin/rv5_js/d-day",
                    name: "build",
                    out: "build/optimized.js"
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-requirejs');

    grunt.loadNpmTasks('grunt-sftp-deploy');

    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.loadNpmTasks('grunt-http-server');

    grunt.loadNpmTasks('grunt-text-replace');

    grunt.loadNpmTasks('grunt-curl');

    grunt.loadNpmTasks('grunt-zip');

    grunt.loadNpmTasks('grunt-accessibility');

    grunt.loadNpmTasks('grunt-jslint');

    grunt.loadNpmTasks("grunt-jsbeautifier");

    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.loadNpmTasks('grunt-contrib-sass');

    grunt.registerTask('default', ['images', 'local', 'http-server']);

    grunt.registerTask('dev', ['replace:dev', 'sass', 'uglify']);

    grunt.registerTask('local', ['replace:local', 'sass', 'uglify']);

    grunt.registerTask('cdn', ['replace:cdn', 'sass', 'uglify']);

    grunt.registerTask('images', ['curl', 'unzip']);

};
